(function ($) {
    $('.btn-hamb').on('click', function (e) {
        e.preventDefault();
        $('.main-nav').toggleClass('collapsed');
        $('.overlay').toggleClass('hidden');
    })

    $('.list-careers a').click(function (e) {
        e.preventDefault();
        $('.popup').show();
    });
    $('.popup').click(function (e) {
        if ($(e.target).is($('.popup'))) {
            e.preventDefault();
            $('.popup').hide();
        }
    });
})(jQuery);
